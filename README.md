# Coqide with Jupyter
This project contain a dockerfile and a docker-compose for running coq into a jupyter notebook.
![Alt text](jupyter_new.png?raw=true "Image with jupyter and list of kernels")

```
docker-compose up
firefox http://localhost:8888
```
It is also pushed to [DockerHub](https://hub.docker.com/r/nicolalandro/multilanguage-jupyter).

# List of kernel
* Python3
* [SoS (Plugin for multi-lenguage into one Notebook)](https://vatlab.github.io/sos-docs/)
* [Coq (Proof assistant)](https://github.com/EugeneLoy/coq_jupyter)
* [Java](https://github.com/SpencerPark/IJava)
* [Scala](https://almond.sh/)
* [Ruby](https://github.com/sciruby/iruby)
* [Javascript](https://github.com/n-riesco/ijavascript)

